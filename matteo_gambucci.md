Matteo Gambucci 4A INF 

comandi bash:

1-diff

Compara file, ed elenca le loro differenze.
Compares files, and lists their differences.

es. diff filename1 filename2

2-find
Cerca file all'interno di una directory.
Find files in directory.

es. find directory options pattern

3-head
Scrive a video le prime 10 righe di un file.
Outputs the first 10 lines of file.

es. head filename