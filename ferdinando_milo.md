Ferdinando Milo 4°A inf

comandi bash


-1 wc
Dice quante righe, parole e caratteri ci sono in un file.

Comando:

 wc nomefile.estensione 

Esempio:

 $  wc dolci.txt
 3  5 35 dolci.txt

Il file in questione, scelto tra diversi che possiedo,
ha 3 righe, 5 parole e 35 caratteri.

-2 head
Compone le prime 10 righe di un file, se non specificato, 
altrimenti posso farlo aggiungendo al comando -n seguito
da il numero delle righe che chiedo.

Comando:

 head nomefile.estensione

Esempio senza specificare:
 $ head dolci.txt
 lista dei dolci
 torta
 bign▒
 cornetti
 biscotti
 ingredienti
 cacao
 zucchero
 sale
 acqua

Esempio con specificazione -n 9 ( 9 = numero di righe):
 $ head -n 9 dolci.txt
 lista dei dolci
 torta
 bign▒
 cornetti
 biscotti
 ingredienti
 cacao
 zucchero
 sale

-3 nl
Mostra il file numerando le righe.

Comando:

 nl nomefile.estensione

Esempio:
 nl dolci.txt
     1  lista dei dolci
     2  torta
     3  bign▒
     4  cornetti
     5  biscotti
     6  ingredienti
     7  cacao
     8  zucchero
     9  sale
    10  acqua
    11  farina
    12  uova

