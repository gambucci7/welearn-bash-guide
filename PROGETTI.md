# Progetti 4AI - script bash

* Per ogni progetto che segue creare uno script bash. Partendo da quello che ritenete più facile/interessante
* Dovete immaginarvi questa situazione:
  - siete seduti di fronte ad un PC con il sistema operativo Ubuntu appena installato
  - installate `git` (con `sudo apt install git`)
  - eseguite `git clone` del nostro repository,
  - eseguite gli script bash che realizzate in questa pagina
  e la vostra macchina (o meglio il vostro sistema operativo) è in grado di fare una cosa in più

* Gli script vanno [creati in una directory](https://gitlab.com/lessons-itis/welearn-bash-guide/#b-mkdir) `scripts/nome_cognome/`
* Per ogni progetto vi chiedo di inserire almeno una personalizzazione a vostro piacimento. Ad esempio:
  - un parametro preso da riga di comando
  - la visualizzazione di un output con il comando "echo"

### Provare gli script su una macchina Debian

Vi ho creato un accesso su una macchina Debian della scuola raggiungibile con la riga di comando:

`ssh nome.cognome@lab.iismerlonimiliani.it -p 22222`

oppure tramite PUTTY software libero e gratuito funzionante sia su Windows che su sistemit GNU/Linux.

## Progetto 1: riprodurre files mp3 e ogg da riga di comando

### Conoscenze richieste
* eseguire un comando da amministratore
* pacchetti GNU/Linux
* ciclo for

### Creare uno script my-music.sh che

- aggiorni la lista pacchetti con `sudo apt update`
- con un ciclo for installi i programmi `mpg123` (o `mpg321`) E `ogg123`
- con un altro ciclo for riproduca un file .mp3 e un file .ogg presente sul vostro disco o nel vostro pendrive. I nomi dei 2 file possono essere passati da riga di comando

#### NOTA1
se mettiamo i 2 cicli for senza controlli otterremo un errore quando il riproduttore mp3 tenta di riprodurre un file .ogg e viceversa. Non è un problema, l'esecuzione continua dopo l'errore e viene invocato il comando ogg123.

#### SPUNTO1
per chi volesse distinguere i tipi dei file è possibile con un comando apposito ([alcuni di voi lo hanno studiato tra questi](https://gitlab.com/lessons-itis/welearn-bash-guide/#11-file-operations)), usare sulla stessa riga il `|` (pipe) per mandare l'output del comando usato al comando `grep` e poi mandare l'output di grep (stessa riga) con `|` al comando apposito per contare le righe di testo ([`wc -l`](https://gitlab.com/lessons-itis/welearn-bash-guide/#m-wc)).

#### SPUNTO2
per chi volesse evitare l'errore alla NOTA1 può mettere il risultato di SPUNTO1 in una variabile usando la "command substitution" (pag. 111) e poi usare l'`if` su quella variabile (ad es: `if [ $is_mp3 -eq 0 ]; then` ... poi a capo il codice e alla fine del ciclo `fi`)

## Progetto 2 my-music-download.sh : scaricare musica libera da Internet

### Conoscenze richieste
* scaricare un file da statico da riga di comando con `wget`
* ciclo for

### Creare uno script my-music-download.sh che

- aggiorni la lista pacchetti con `sudo apt update`
- installi il programma `wget`
- scarichi tutti i files i cui nomi sono passati da riga di comando dal sito https://sampleswap.org/mp3/creative-commons/free-music.php (trovare l'URL preciso di ogni file con tasto destro -> Copia Indirizzo) o dal portale di musica Creative Commons  [Jamendo](https://www.jamendo.com) (richiede creazione account e test)
- li riproduca con i programmi installati nel progetto precedente

#### NOTA1
Per usare in uno script tutti i parametri passati da riga di comando utilizzare la variabile `$@`(nel libro viene citata `$*` a questo scopo a fine pag. 104 e inizio 105, va bene anche `$*`. Fate un semplice script contenente `echo $@` e `echo $*` per testarne il funzionamento lanciandolo da riga di comando con più parametri (ad es: 3 o 4)


## Progetto 3 my-midi.sh: far suonare MIDI nella propria postazione

### Conoscenze richieste

* eseguire un comando come amministratore (`sudo`)
* pacchetti GNU/Linux Ubuntu:
    * aggiornare la lista (`apt update`)
    * installare un pacchetto (`apt install <nome pacchetto>`)
* [ciclo `for`](https://gitlab.com/lessons-itis/welearn-bash-guide/#26-loops) (al link la seconda notazione. Sul libro trovate il ciclo for a pag. 117)

### Creare uno script my-midi.sh che
- aggiorni la lista pacchetti
- installi (uno per volta con un ciclo for) i pacchetti `fluidsynth` e `vmpk`

Per verificare se lo script ha fatto il suo dovere:

- avviare `qsynth`
- selezionare "Setup..." -> "Soundfonts" -> "Open" e scegliere il soundfont proposto
- avviare `vmpk`
- cliccare su "Edit" -> "Connections" e su output "FluidSynth"
- suonare il piano e vedere se il proprio sistema è in grado di riprodurre suoni MIDI.

## Progetto 4: config-user.sh per configurare il proprio utente

### Conoscenze richieste

* home di ogni utente (le impostazioni di ogni utente sono nella sua directory `/home/<nome utente>`
* command substitution (pag 102)
* ciclo for (pag. 117)

### Creare uno script config-user.sh che

creare un file .vimrc contenente la riga `colorscheme ron` o il [colorscheme desiderato](https://alvinalexander.com/linux/vi-vim-editor-color-scheme-colorscheme)
copiare il file .vimrc per tutti gli utenti, i cui nomi si trovano nella directory `/home`

