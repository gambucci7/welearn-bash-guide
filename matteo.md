3 comandi git bash

gzip
Il comando gzip comprime i file.
Ogni singolo file è compresso in un singolo file che li contiene tutti quanti.

Sintassi :

 gzip [Opzione] [nome_file]

esempio:

gzip mydocument.odt





echo
echo è un comando dei sistemi operativi Unix e Unix-like, e più in generale dei sistemi POSIX che scrive i suoi parametri sullo standard output.
È solitamente usato negli script di shell per visualizzare messaggi informativi e/o per scrivere del testo in un file.
 Sintassi :

 echo [opzione] [stringa]

esempio:

$ echo Questo è un testo.
Questo è un testo.





grep
Viene utilizzato per ricercare una determinata stringa di caratteri in un file . 
Stringa è la parola o la frase da ricercare e file è il nome del file nel quale deve essere eseguita la ricerca.

Sintassi:
grep [stringa] [file]

esempio
$ grep Allan interni
David Allan         x76438
Edgar Allan Poe     x72836
$ grep Al interni
Louisa May Alcott   x74236
David Allan         x76438
Edgar Allan Poe     x72836
$
